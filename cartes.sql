-- Création de la base de données cartesBDD

DROP database if EXISTS cartesBDD;
CREATE database cartesBDD;
USE cartesBDD;

-- Création de la table cartes

CREATE TABLE cartes (
  id int not null auto_increment primary key,
  description varchar(255) not null,
  date int(11) not NULL
);

-- Contenu de la table `cartes`

INSERT INTO `cartes` (`description`, `date`) VALUES
('Début 1re guerre mondiale', 1914),
('Chute du mur de Berlin', 1989),
('Début 2e guerre mondiale', 1939),
("Découverte de l'Amérique", 1492),
('Révolution Française', 1789),
('1er pas sur la lune', 1969),
('Passage à la Ve république', 1958),
('Droits de vote des femmes en France', 1944),
("Début de La guerre d'Irak, ou seconde guerre du Golfe", 2003),
('Défaite de Waterloo', 1815),
('Début de la guerre de 100 ans', 1337),
("Sacre de l'Empereur Charlemagne", 800),
('Fin de la construction de la tour Eiffel', 1889),
("Début de la guerre d'indépendance des États-Unis", 1775),
("Indépendance de l'Inde", 1947),
('Début de la guerre de Sécession des États-Unis', 1861);

DROP USER  if exists 'admin'@'127.0.0.1';
CREATE USER 'admin'@'127.0.0.1' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON cartesBDD.* TO 'admin'@'127.0.0.1';
