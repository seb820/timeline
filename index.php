<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <script>
        <?php include 'model.php'; ?>
        const cartes = JSON.parse(`<?php echo json_encode(readAll()); ?>`)
    </script>
    <script type='module' src="timeline.js"></script>
</body>
</html>