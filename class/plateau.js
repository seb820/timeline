export class Plateau{
    constructor(){
        this.table = []
    }
    insertArrayAt(array, index, arrayToInsert) {
        Array.prototype.splice.apply(array, [index, 0].concat(arrayToInsert));
        return array;
    }
    insertAt(array, index) {
        var arrayToInsert = Array.prototype.splice.apply(arguments, [2]);
        return insertArrayAt(array, index, arrayToInsert);
    }

    
    positioner(carte,pos){
        this.table.splice(carte, 0, pos);
    }
}