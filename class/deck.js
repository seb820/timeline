class Deck {
    constructor() {
      this.cartes
      for (let i = 1; i <= 16; i++) {
        this.cartes.push('carte' + i);
      }
    }
    create(arr){ 
      this.cartes= new Array(arr)
    }
    shuffle() {
      for (let i = this.cartes.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [this.cartes[i], this.cartes[j]] = [this.cartes[j], this.cartes[i]];
      }
    }
  
    piocher() {
      return this.cartes.pop();
    }
  
    distribuer(main) {
      for (let i = 0; i < 4; i++) {
        main.cartes.push(this.piocher());
      }
    }
  }
  
  class Main {
    constructor() {
      this.cartes = [];
    }
  }
  
  const deck = new Deck();
  console.log(deck.cartes); // ['carte1', 'carte2', 'carte3', ..., 'carte16']
  deck.shuffle();
  console.log(deck.cartes); // par exemple ['carte6', 'carte15', 'carte1', ..., 'carte9']
  
  const main = new Main();
  deck.distribuer(main);
  console.log(main.cartes); // ['carte9', 'carte6', 'carte12', 'carte2', 'carte10']
  