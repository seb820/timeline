import { Carte } from "./class/carte"
import { Hand } from "./class/main"
import { Plateau } from "./class/plateau"
/* if la date de la carte posée est plus grande que la carte à index +1 du plateau
OU plus petite que la carte à index -1: piocher une carte */

function checkPlacePlateau(plateau, cartePosee, deck){
    if ((plateau.indexOf(cartePosee - 1).date > plateau.indexOf(cartePosee).date) || 
        (plateau.indexOf(cartePosee + 1).date < plateau.indexOf(cartePosee).date)){
        deck.piocher()
    }
}